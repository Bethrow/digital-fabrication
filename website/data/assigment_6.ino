// C++ code
//
const int BUTTON_pin = 2;
const int BUTTON2_pin= 5;
bool BUTTON_status = false;
bool BUTTON2_status = false;
bool BUTTON_temp_Status = BUTTON_status; //temp Variable that caches Button Status 

const int LEDs_Pin_length = 3;
int LEDs_Pin[] = {13,12,11};
bool LED_Status[] = {false,false,false};
int currentIndex = 0;

void setup()
{
  //SERIAL PRINT 
  Serial.begin(9600); // open the serial port at 9600 bps:
  
  pinMode(BUTTON_pin, INPUT); // Input as Button
  pinMode(BUTTON2_pin, INPUT); // Input as Button
  
  // Button Setup
  for(int i = 0; i < LEDs_Pin_length; i++){
    pinMode(LEDs_Pin[i], OUTPUT);
  }
}

void loop()
{
  BUTTON_status = (digitalRead(BUTTON_pin)== HIGH);
  BUTTON2_status = (digitalRead(BUTTON2_pin)== HIGH);
  
  if(BUTTON2_status){
    currentIndex++;
    currentIndex %= LEDs_Pin_length;
    delay(100);
    Serial.println("Butten is Selected "+String(currentIndex));
  }
  
  // When Button is hold down, it shouldnt switch between on and off
  if(BUTTON_status && BUTTON_temp_Status != BUTTON_status ){
    Serial.println("Butten is Pressed ");
    setLED(currentIndex);
    delay(100);
  }
  BUTTON_temp_Status = BUTTON_status;
}

void setLED(const int& index){
  // False == LOW; true == HIGH : 
  int status= map(!(LED_Status[index]), false, true, LOW,HIGH);
  digitalWrite(LEDs_Pin[index], status);
  LED_Status[index] = !LED_Status[index];
}
