// C++ code
// 20, 27 , 3f
//#include <Wire.h>
#include <Servo.h>
#include <LiquidCrystal_I2C.h>

int SERVO_pin = 5;
int BUTTON_pin = 2;
int POTENTIOMETER_pin = A3;
int LCD_I2C_adress = 0x27;
  
LiquidCrystal_I2C lcd(LCD_I2C_adress,16,2);
Servo servo;

void setup()
{
  //Wire.setSCL(9); // Setup Pin for Comunication
  //Wire.setSDA(8); // Setup Pin for Comunication
  servo.attach(SERVO_pin, 500, 2500);
  
  lcd.init();
  lcd.backlight();
  lcd.setCursor(3,1);// 3 => Char 3 ; 1 => Linen
  lcd.print("Starting up");
  
  //BUTTON
  pinMode(BUTTON_pin,INPUT);
  
  //POTENTIOMETER
  pinMode(POTENTIOMETER_pin, INPUT);
  
  Serial.begin(9600);
}

void loop()
{ 
  // Servo //
  // ANALOG READ
  int deg = map(analogRead(POTENTIOMETER_pin), 0,1023, 0, 180);
  // WRITE SERVO
  setMotorPosition(deg);
  delay(1000);
  // Button //
  //bool BUTTON_status = (LOW == digitalRead(BUTTON_pin));
  if(digitalRead(BUTTON_pin) == LOW){
    setDisplayButton();
    delay(100);
  }else{
    setDisplayPotentiometer(deg);
  }
}

void setDisplayButton(){
   lcd.clear();
   lcd.setCursor(0,1);
   lcd.print("Button pressed");
   Serial.println("Button Was Pressed");
}

void setDisplayPotentiometer(int &value){
   lcd.clear();
   lcd.setCursor(0,0);
   lcd.print(value);
   Serial.println(value);
}

void setMotorPosition(const int &deg){
  servo.write(deg);
}
/*

#include <Wire.h>

// Set I2C bus to use: Wire, Wire1, etc.
#define WIRE Wire1

void setup() {
  WIRE.begin();

  Serial.begin(9600);
  while (!Serial)
     delay(10);
  Serial.println("\nI2C Scanner");
}


void loop() {
  byte error, address;
  int nDevices;

  Serial.println("Scanning...");

  nDevices = 0;
  for(address = 1; address < 127; address++ ) 
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    WIRE.beginTransmission(address);
    error = WIRE.endTransmission();

    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address<16) 
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");

      nDevices++;
    }
    else if (error==4) 
    {
      Serial.print("Unknown error at address 0x");
      if (address<16) 
        Serial.print("0");
      Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");

  delay(5000);           // wait 5 seconds for next scan
}
/**/

